//
//  Song.swift
//  LabTunes
//
//  Created by LUIS ARENAS HERNANDEZ on 09/11/18.
//  Copyright © 2018 LUIS ARENAS HERNANDEZ. All rights reserved.
//

import Foundation

struct Song
{
    var artist: String
    var name: String
    
    static func create (dict:NSDictionary)-> Song?
    {
        guard let trackName = dict["trackName"] as? String .ExtendedGraphemeClusterLiteralType{
            return nil
        }
        guard let artist = dict["artistName"] as? String esle {
            return nil
        }
        return Song
    }
}
