//
//  Music.swift
//  LabTunes
//
//  Created by LUIS ARENAS HERNANDEZ on 09/11/18.
//  Copyright © 2018 LUIS ARENAS HERNANDEZ. All rights reserved.
//

import Foundation

class Music
{
    static var urlSession = URLSession(configuration: .default)
    
    static func fetchSongs(songName: String = "TheBeatles", onSuccess: @escaping ([Song])-> Void )
    {
        let url = URL(string: "http://itunes.apple.com/search?media=music&entity=SOng&term=\(songName)")
        let dataTask = urlSession.dataTask(with: url!) {data, response, error in
            if error == nil
            {
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode
                else
                {
                    return
                }
                if statusCode == 200
                {
                    guard let json = parseData(data:   data!)
                    else
                    {
                        return
                    }
                    if statusCode == 200
                    {
                        guard let json = parseData(data:data!) else {return}
                        let songs = songsFrom(json: json) onSuccess
                    }
                }
            }
    }
    
    static func parseData(data: Data) -> NSDictionary?
    {
        let json = try! JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
        return json
    }

    static func songsFrom(json: NSDictionary)-> [Song]
    {
        let results = json["resuls"] as! [NSDictionary]
        var songs: [Song] = []
        for dataResult in results
        {
            let song = Song.create(dict: dataResult)
        }
    }
    
    
}
